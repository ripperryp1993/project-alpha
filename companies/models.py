from django.db import models
from projects.models import User

# Create your models here.


class Company(models.Model):
    name = models.CharField(max_length=50)
    planner = models.ForeignKey(
        User, related_name="companies", null=True, on_delete=models.SET_NULL
    )
    projects = models.ManyToManyField(
        "projects.Project", related_name="companies"
    )

    def __str__(self):
        return self.name
