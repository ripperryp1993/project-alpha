from django.urls import path

from companies.views import (
    CompanyCreateView,
    CompanyDetailView,
    CompanyListView,
    CompanyUpdateView,
)


urlpatterns = [
    path("", CompanyListView.as_view(), name="list_companies"),
    path("<int:pk>/", CompanyDetailView.as_view(), name="show_company"),
    path("create/", CompanyCreateView.as_view(), name="create_company"),
    path(
        "<int:pk>/update/", CompanyUpdateView.as_view(), name="update_company"
    ),
]
