# from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from companies.models import Company

# Create your views here.


class CompanyListView(LoginRequiredMixin, ListView):
    model = Company
    template_name = "companies/list.html"
    paginate_by = 5

    def get_queryset(self):
        return Company.objects.filter(planner=self.request.user)


class CompanyDetailView(LoginRequiredMixin, DetailView):
    model = Company
    template_name = "companies/detail.html"

    def get_queryset(self):
        return Company.objects.filter(planner=self.request.user)


class CompanyCreateView(LoginRequiredMixin, CreateView):
    model = Company
    template_name = "companies/create.html"
    fields = ["name", "projects"]

    def form_valid(self, form):
        company = form.save(commit=False)
        company.planner = self.request.user
        company.save()
        form.save_m2m()
        return redirect("show_company", pk=company.id)


class CompanyUpdateView(LoginRequiredMixin, UpdateView):
    model = Company
    template_name = "companies/update.html"
    fields = ["name", "projects"]

    def get_queryset(self):
        return Company.objects.filter(planner=self.request.user)

    def get_success_url(self):
        return reverse_lazy("show_company", args=[self.object.id])
