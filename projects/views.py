from django.shortcuts import render
from django.urls import reverse_lazy
from projects.models import Project
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from tasks.models import Task

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    paginate_by = 10

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


def search_project(request):
    if request.method == "POST":
        searched = request.POST["searched"]
        projects = Project.objects.filter(name__contains=searched)
        tasks = Task.objects.filter(name__contains=searched)
        return render(
            request,
            "projects/search.html",
            {"searched": searched, "projects": projects, "tasks": tasks},
        )
    else:
        return render(request, "projects/search.html", {})
